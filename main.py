import argparse
import os
import ndjson
import sys
import nltk



def read_ads_stream_from_std_in():
    reader = ndjson.reader(sys.stdin)
    for ad in reader:
        yield ad


def read_ads_from_file(rel_filepath):
    currentdir = os.path.dirname(os.path.realpath(__file__)) + os.sep
    # Open input file with ads and load ndjson to dictionary...
    ads_path = currentdir + rel_filepath
    with open(ads_path, encoding="utf8") as ads_file:
        ads = ndjson.load(ads_file)
    return ads


def print_ad(ad):
    output_json = ndjson.dumps([ad], ensure_ascii=False)
    print(output_json, file=sys.stdout)

def print_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def tokenize_to_sentences(ad_description):
    # some ads have job description in brackets
    if isinstance(ad_description, list):
        ad_description = ad_description[0]

    all_sentences = []
    sections = ad_description.split('\n')
    for section in sections:
        sentences = swedish_punkt_tokenizer.tokenize(section)
        sentences = [sentence for sentence in sentences if sentence]
        all_sentences.extend(sentences)
    return all_sentences


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Extracts a brief description for the ad')
    parser.add_argument('--filepath', help='Optional relative filepath to read ads from', required=False)

    args = parser.parse_args()
    filepath = args.filepath

    if filepath:
        ads = read_ads_from_file(filepath)
    else:
        ads = read_ads_stream_from_std_in()

    swedish_punkt_tokenizer = nltk.data.load('nltk:tokenizers/punkt/swedish.pickle')

    for ad in ads:
        try:
            if 'originalJobPosting' in ad and 'description' in ad['originalJobPosting']:

                ad_description = ad['originalJobPosting']['description']

                all_sentences = tokenize_to_sentences(ad_description)

                if all_sentences:
                    brief_description = ' '.join(all_sentences[:2]).strip()
                else:
                    brief_description = ''

                ad['brief_description'] = brief_description
            else:
                ad['brief_description'] = ''

            print_ad(ad)

        except:
            e = sys.exc_info()[0]
            print_error(e)
