FROM docker.io/library/ubuntu:22.04 AS base

WORKDIR /app

COPY requirements.txt requirements.txt
COPY main.py run.sh /app/
COPY resources /app/resources

# TODO: why is nltk.punkt-tokenizer installed outside of requirements.txt?

RUN apt-get -y update \
    && apt-get -y install python3 python3-pip \
    && python3 -m pip install -r requirements.txt \
    && pip3 install nltk \
    && python3 -c "import nltk; nltk.download('punkt', download_dir='/usr/share/nltk_data')"

CMD ["python3","main.py"]
